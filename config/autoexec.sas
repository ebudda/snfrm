/* 
 * autoexec_usermods.sas
 *
 *    This autoexec file extends autoexec.sas.  Place your site-specific include 
 *    statements in this file.  
 *
 *    Do NOT modify the autoexec.sas file.  
 *    
 */

%include "/hipaa/0211942.200_MIDS_PGM/Readmissions/pgm/ebudda/config/macrolib.sas"/nosource2;

%macrolib(%bquote(/hipaa/0211942.200_MIDS_PGM/Readmissions/pgm/ebudda/programs/macros))

