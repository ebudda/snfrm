%macro macrolib (source);

options mprint mlogic;

Filename filelist pipe "dir &source./*.sas";

Data flist;                                        
Infile filelist truncover;
Input filename $100.;
if index(filename,'.sas');

Run; 

proc sql noprint;
select distinct filename
into :var_input separated by ','
from flist;
quit

%let inputval = %sysfunc(countw("&var_input",','));

%do i = 1 %to &inputval;
  
   %let inputvalue = %scan(%bquote(&var_input),&i,',');

   %put &=inputvalue;
 
   %include "&inputvalue"/nosource2;

   run;

%end;

%mend macrolib;

