%macro compare(newtable=, 
               newpath=,
	       newpathlib=,
	       oldtable=,
	       oldpath=,
	       unzip=N,
               debug=N);

%libbuild(testname,%bquote(&_temp.),rand=y)

%if %upcase(&unzip) = Y %then %do;
   
   %copysource(newpath=&_pathtemp,
               oldtable=&oldtable,
               oldpath=&oldpath,
               sourcetype=sas7bdat.7z)
			   
   %fileunzip(oldtable=&oldtable,
              newpath=&_pathtemp)

%end;

%else %if %upcase(&unzip) = N %then %do;

   %copysource(newpath=&_pathtemp,
               oldtable=&oldtable,
               oldpath=&oldpath,
               sourcetype=sas7bdat)

%end;

proc compare base=&_nametemp..&newtable compare=&newpathlib..&newtable out=&_nametemp..fullcompare OUTNOEQUAL OUTBASE OUTCOMP OUTDIF NOPRINT; 

run;

proc contents data=&_nametemp..&newtable out=&_nametemp..temp1(keep=name type length Label) noprint;
proc contents data=&newpathlib..&newtable out=&_nametemp..temp2(keep=name type length Label) noprint;

proc sql noprint;
create table &_nametemp..temp as
select 
   a.name as base_name,
   b.name as comp_name,
   case when a.name ^= b.name then 1
   else 0 
   end as flag_name,
   a.type as base_type,
   b.type as comp_type,
   case when a.type ^= b.type then 1
   else 0 
   end as flag_type,
   a.length as base_length,
   b.length as comp_length,
   case when a.length ^= b.length then 1
   else 0 
   end as flag_length,
   a.Label as base_Label,
   b.Label as comp_Label,
   case when a.Label ^= b.Label then 1
   else 0
   end as flag_Label
from &_nametemp..temp1 a
full join &_nametemp..temp2 b on a.name = b.name;
quit;

data &_nametemp..varcompare;
set &_nametemp..temp;
if sum(of flag_:) > 0;

run;

proc contents data=&_nametemp..fullcompare out=runrpt.&newtable._f_&_fldate. (keep=nobs) noprint;
proc contents data=&_nametemp..varcompare out=runrpt.&newtable._v_&_fldate. (keep=nobs) noprint;

%if %upcase(&debug) = N %then %do;
data _null_;
call system ("rm -r &_pathtemp.");

run;

%end;

%mend compare;