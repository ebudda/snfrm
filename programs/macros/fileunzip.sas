%macro fileunzip(oldtable=,
                 newpath=)
                 ;

data _null_;
call system ("cd &newpath");
call system ("7za e &oldtable..sas7bdat.7z");
%let cycles = 0;
%processwait(filecheck=&newpath.&_sl.&oldtable..sas7bdat.7z);

run;

%mend fileunzip;