%macro processwait(filecheck=,waittime=20);

data _null_;
rc=sleep(&waittime.,1);
rc=fileexist("&filecheck");
call symputx("fexist",rc);

run;
%put &=fexist;
%put &=cycles;
%if &fexist = 0 and &cycles <16 %then %do;
   %let cycles = %eval(&cycles+1);
   %processwait(filecheck=&filecheck);
%end;
%else %if &fexist = 0 and &cycles > 16 %then %do;
    %put "ERROR: Wait time exceeded for &filecheck";
%end;
%else %if &fexist = 1 %then %do;
    %put "NOTE: Process complete for &filecheck";
%end;	
	
%mend processwait;