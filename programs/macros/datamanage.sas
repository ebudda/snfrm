%macro datamanage(action=print,lib=,dataset=);

proc sql noprint;
   select count(memname) into :dcount 
   from dictionary.members
   where upcase(libname)=%upcase("&lib")
         and memtype='DATA' 
   %if %superq(dataset) ^= %then %do;
         and upcase(memname) = %upcase("&dataset")
   %end;
   ;
quit;

%put NOTE: Total Members found in &lib is &dcount.;

%if &dcount = 0 %then %put NOTE: Library &lib is empty, no further actions will be taken.;

%if &dcount > 0 %then %do;
   %if %upcase(&action) = PRINT %then %do;
      proc datasets library=%upcase(&lib) memtype=data;

      run;
   %end;
   %if %upcase(&action) = DROP %then %do;
      proc datasets library=%upcase(&lib) kill;

      run;

      proc sql noprint;
      select count(memname) into :dcount 
      from dictionary.members
      where upcase(libname)=%upcase("&lib")
            and memtype='DATA' 
      %if %superq(dataset) ^= %then %do;
            and upcase(memname) = %upcase("&dataset")
      %end;
      ;
      quit;

      %put "Total Members found in &lib is &dcount.";
      %if &dcount = 0 %then %put NOTE: Library &lib has been cleared.;
      %if &dcount > 0 %then %put ERROR: Library &lib has NOT been cleared. Validate that you have access to clear this library.;

   %end;
%end;

%mend datamanage;
