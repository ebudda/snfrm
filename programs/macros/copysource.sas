%macro copysource(newpath=,
                  oldtable=,
                  oldpath=,
				  sourcetype=);

data _null_;
call system ("cp &oldpath.&_sl.&oldtable..&sourcetype &newpath");

run;
%let cycles = 0;
%processwait(filecheck=&newpath.&_sl.&oldtable..&sourcetype);

run;

%mend copysource;