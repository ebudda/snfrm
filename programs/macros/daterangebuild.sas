%macro daterangebuild(yr);

%do i = 1 %to 4;
   %global fyqtrb&yr.&i. fyqtre&yr.&i. cyqtrb&yr.&i. cyqtre&yr.&i.;
   %let fyqtrb&yr.&i. = %sysfunc(PUTN(%sysfunc(intnx(qtr,%sysfunc(mdy(01,01,&yr)),%eval(&i-2),b)),yymmddn8.),yymmddn8.);
   %let fyqtre&yr.&i. = %sysfunc(PUTN(%sysfunc(intnx(qtr,%sysfunc(mdy(01,01,&yr)),%eval(&i-2),e)),yymmddn8.),yymmddn8.);
   %let cyqtrb&yr.&i. = %sysfunc(PUTN(%sysfunc(intnx(qtr,%sysfunc(mdy(01,01,&yr)),%eval(&i-1),b)),yymmddn8.),yymmddn8.);
   %let cyqtre&yr.&i. = %sysfunc(PUTN(%sysfunc(intnx(qtr,%sysfunc(mdy(01,01,&yr)),%eval(&i-1),e)),yymmddn8.),yymmddn8.);
%end;

%mend daterangebuild;