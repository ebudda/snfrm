%macro libbuild(name,path,rand=N,ronly=N);
%global _nametemp _pathtemp;
%if %upcase(&rand) = Y %then %do;
   %let _nametemp=temp;
   %let _pathtemp = &path.&_sl.&_nametemp;
   %do %until (%sysfunc(fileexist(&_pathtemp)) ^= 1);
      %if %symexist(_ncount) %then %let _ncount=%eval(&_ncount+1);
      %else %let _ncount = 1;
      %let _nametemp = temp&_ncount;
	  %let _pathtemp = &path.&_sl.&_nametemp;
   %end;
   data _null_;
   call system ("mkdir &_pathtemp");
   run;
   %if %sysfunc(libref(&_nametemp)) ^= 0 %then %do;
   %put Library &_nametemp does not exist, building...;
   libname &_nametemp "&_pathtemp" FILELOCKWAIT=600
   %if %upcase(&ronly) = Y %then %do;
      access=readonly
   %end;
   ;
   %if %sysfunc(libref(&_nametemp)) ^= 0 %then %do; 
      %put Library &_nametemp could not be built;
   %end;
   %else %if %sysfunc(libref(&_nametemp)) = 0 %then %put Library &_nametemp has been created.;
   %end;
   %else %put Library &_nametemp exists.;
%end;  
%else %if %upcase(&rand) ^= Y %then %do;
   %if %sysfunc(libref(&name)) ^= 0 %then %do;
      %put Library &name does not exist, building...;
      libname &name "&path" FILELOCKWAIT=600
      %if %upcase(&ronly) = Y %then %do;
         access=readonly
      %end;
      ;
      %if %sysfunc(libref(&name)) ^= 0 %then %do; 
	     %put Library &name could not be built;
	  %end;
	  %else %if %sysfunc(libref(&name)) = 0 %then %put Library &name has been created.;
   %end;
%end;
%else %put Library &name exists.;

%mend libbuild;