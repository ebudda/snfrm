%macro metaset(build=CHECK,var_input=NULL);

/* CHECK: Validate that the table var.metaset is available   */
/* REBUILD: Rebuild Table                                    */
/* LENGTH: Set Length of Variable                            */
/* LABEL: Set Label of Variable                              */

%if %upcase(&build) = CHECK %then %do;

   proc sql noprint;
      select count(memname) into :dcount 
      from dictionary.members
      where upcase(libname)="VAR"
            and memtype='DATA' 
            and upcase(memname) = "METASET"
      ;
   quit;

%end;
%else %do;

   %let dcount = 1; 

%end;

%if &dcount = 0 or %upcase(&build) = REBUILD %then %do;

   data var.metaset;
   infile "&_var.&_sl.variables.csv" dlm=',' dsd truncover firstobs=2;
   input Variable :$32. Type :$1. Length :8. Label :$255.;
   run;
%end;

%if %upcase(&build) ^= REBUILD and %upcase(&build) ^= CHECK %then %do;

   %let mydataid=%sysfunc(open(var.metaset,i));
   %let nobs =%sysfunc(attrn(&mydataid,nobs));
   %let rc = %sysfunc(close(&mydataid));

   %put &nobs;

   %let inputval = %sysfunc(countw("&var_input",','));

   %do z = 1 %to &inputval;
      %let var = %scan(%bquote(&var_input),&z);
      %let _end = 0;
      %do j = 1 %to &nobs;
         %let mydataid=%sysfunc(open(var.metaset,i));
         %let rc=%sysfunc(fetchobs(&mydataid,&j));
         %let Variable=%sysfunc(getvarc(&mydataid,%sysfunc(varnum(&mydataid,Variable))));
         %put &Variable;
         %put &var;    
         %if %upcase(&Variable) = %upcase(&var) %then %do;
            %if %upcase(&build) = LENGTH or %upcase(&build) = ALL %then %do;
               %let Type=%sysfunc(getvarc(&mydataid,%sysfunc(varnum(&mydataid,Type))));
               %let Length=%sysfunc(getvarn(&mydataid,%sysfunc(varnum(&mydataid,Length))));
               length &Variable &Type. &Length. %str(;);
            %end;
            %if %upcase(&build) = LABEL or %upcase(&build) = ALL %then %do;
               %let Label =%sysfunc(getvarc(&mydataid,%sysfunc(varnum(&mydataid,LABEL))));
               label &Variable = "&Label"  %str(;);
            %end;
            %if %upcase(&build) = FORMAT or %upcase(&build) = ALL %then %do;
               %let Label =%sysfunc(getvarc(&mydataid,%sysfunc(varnum(&mydataid,LABEL))));
               format &Variable &Type.&Length.. %str(;);
            %end;
            %let _end = 1;
         %end;
         %let rc=%sysfunc(close(&mydataid));
      %end;
   %end;
%end;

%mend metaset;
