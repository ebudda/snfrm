%macro fileQC(_runtype=,_table=,_where=,_format=,_obs=10,_var1=,_var2=);

%if %upcase(&_runtype) = FREQ %then %do;

   proc freq data=&_table(keep= &_var1 &_var2);
   %if %sysevalf(%superq(_where)=,boolean) = 0 %then %do;
      where %unquote(&_where.);
   %end;
   tables &_var1
   %if &_var2 ^= %then %do;
      %str(* &_var2)
   %end;
   / out=temp.temp1 missing list;
  %if %sysevalf(%superq(_format)=,boolean) = 0 %then %do;
      format %unquote(&_format.);
   %end;

   run;

   data temp.temp2 (drop=&_var1 &_var2);
   length tsource variable1 variable2 $60 value1 value2 $200;
   set temp.temp1;
   tsource = "&_table";
   variable1 = "&_var1";
   value1 = left(&_var1);
   %if %sysevalf(%superq(_var2)=,boolean) %then %do;
      variable2 = "";
      value2 = "";
   %end;
   %else %do; 
      variable2 = "&_var2";
      value2 = left(&_var2);
   %end;

   run;

%end;

%else %if %upcase(&_runtype) = PRINT %then %do;

   data temp.temp1;
   set &_table (keep=&_var1 obs=&_obs);
   %if %sysevalf(%superq(_format)=,boolean) = 0 %then %do;
      format %unquote(&_format.);
   %end;
   %if %sysevalf(%superq(_where)=,boolean) = 0 %then %do;
      where %unquote(&_where.);
   %end;

   run;
%end;

%else %if %upcase(&_runtype) = CONTENTS %then %do;

   proc contents data=&_table out=temp.temp1;

   run;

%end;

%else %if %upcase(&_runtype) = MEANS %then %do;

  proc means data=&_table mean min max nmiss;
    var &_var1;
   %if %sysevalf(%superq(_where)=,boolean) = 0 %then %do;
      where %unquote(&_where.);
   %end;
   output out=temp.temp1;

%end;

   data temp.temp2 (drop=&_var1 &_var2);
   length tsource variable1 variable2 $60 value1 value2 $200;
   set temp.temp1;
   tsource = "&_table";
   variable1 = "&_var1";
   value1 = left(&_var1);
   %if %sysevalf(%superq(_var2)=,boolean) %then %do;
      variable2 = "";
      value2 = "";
   %end;
   %else %do; 
      variable2 = "&_var2";
      value2 = left(&_var2);
   %end;

   run;

%mend fileQC;
